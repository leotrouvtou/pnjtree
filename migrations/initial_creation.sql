-- id integer primary key autoincrement,
-- name VARCHAR(100),
-- code_on VARCHAR(100),
-- code_off VARCHAR(100)
-- );
-- drop table if exists cronAction;
-- CREATE TABLE cronAction
-- (
-- id integer primary key autoincrement,
-- label  VARCHAR(100),
-- onhour integer NOT NULL,
-- onminute integer NOT NULL,
-- offhour integer NOT NULL,
-- offminute integer NOT NULL,
-- dayofweek  VARCHAR(100),
-- active boolean DEFAULT True,
-- switch_id INTEGER REFERENCES switchs(id) ON delete CASCADE
-- );
drop table if exists pnj;
CREATE TABLE pnj
(
ID SERIAL PRIMARY KEY,
Name VARCHAR(100),
PhotoURL TEXT DEFAULT '' NOT NULL,
Description TEXT DEFAULT '' NOT NULL,
ParentID integer,
FOREIGN KEY (ParentID) REFERENCES pnj(id)
);


drop table if exists relation;
CREATE TABLE relation
(
ID SERIAL PRIMARY KEY,
name text,
pnj1 integer,
pnj2 integer
);

