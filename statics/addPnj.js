function goAddPnj(id){
  // get all selection

  var name   = document.getElementById("name").value;
  var photoUrl    = document.getElementById("photoUrl").value;
  var description    = document.getElementById("description").value;
  var parentID    = document.getElementById("parentID").value;

  // send all of that shit to the server
  $.ajax({
    type: "POST",
    url: "/addPnj/"+id,
    dataType: "json",
    traditional: true,
    data: {name: name, photoUrl: photoUrl, description: description, parentID : parentID },
    success: function(data){
      console.log(data["HTTPRESPONSE"])
      if (data["HTTPRESPONSE"]=="error"){
        $("#message").html("<strong>Oh snap! </strong> Change a few things up and try submitting again.");
        // show
        $("#message").show();
        $("#message").alert();
        // hide after 3 seconds
        var t = setTimeout("$(\"#message\").hide() ;",3000);
      } else {
        window.location.href = '/pnjdetail/'+id;
      }
    }
  });
}
