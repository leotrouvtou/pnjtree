function goAddRelation(pnjID){
  // get all selection

  var name   = document.getElementById("name").value;
  var pnj2    = document.getElementById("pnj2").value;

  // send all of that shit to the server
  $.ajax({
    type: "POST",
    url: "/addrelation/"+pnjID,
    dataType: "json",
    traditional: true,
    data: {name: name, pnj2: pnj2},
    success: function(data){
      if (data["HTTPRESPONSE"]=="error"){
        $("#message").html("<strong>Oh snap! </strong> Change a few things up and try submitting again.");
        // show
        $("#message").show();
        $("#message").alert();
        // hide after 3 seconds
        var t = setTimeout("$(\"#message\").hide() ;",3000);
      }else {
        window.location.href = '/pnjdetail/'+pnjID;
      }
    }
  });
}
