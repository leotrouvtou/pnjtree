import os
from flask import Flask, abort
from flask import render_template, send_from_directory
from flask import redirect, url_for
from flask import request
import requests, split, json
import sqlite3 as sql
from werkzeug.wsgi import SharedDataMiddleware
import validators
from psycopg2 import connect, extras

app = Flask(__name__)

app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
    '/statics':  os.path.join(os.path.dirname(__file__), 'statics')
})



def write_pg(fd, params=None):
    with open(fd) as fd:
        try:
            postgresql = connect(user= None, host= None, password= None, port= 5432, database='pnjtree')
            with postgresql.cursor(
                    cursor_factory=extras.RealDictCursor) as cur:
                cur.execute(fd.read(), params)
                postgresql.commit()
        except Exception as e:
            postgresql.rollback()
            raise
    postgresql.close()

def request_pg(fd, params=None):
    postgresql = connect(user= None, host= None, password= None, port= 5432, database='pnjtree')
    with open(fd) as fd:
        try:
            with postgresql.cursor(
                    cursor_factory=extras.RealDictCursor) as cur:
                cur.execute(fd.read(), params)
                return cur.fetchall()
        except Exception as e:
            postgresql.rollback()
            raise
    postgresql.close()

@app.route('/')
def index():
    jdr = request_pg('sql/list_jdr.sql')
    return render_template('index.html', pnjs=jdr)

def is_valid(form):
    if (form["photoUrl"]=='' or validators.url(form["photoUrl"])):
        return True
    else:
        return False

def is_valid_relation(form):
    if (form["pnj2"]=='' or form["name"]==''):
        return False
    else:
        return True


@app.route("/listPnj/<nodeId>")
def listPnj(nodeId):
    pnj = request_pg('sql/detail_pnj.sql', (nodeId,))
    pnjs = request_pg('sql/list_pnj.sql', (nodeId,))
    return render_template('listPnj.html', pnjs=pnjs, pnj=pnj[0])

@app.route("/graphPnj/<nodeId>")
def graphPnj(nodeId):
    pnj = request_pg('sql/detail_pnj.sql', (nodeId,))
    return render_template('graph.html',pnj=pnj[0])

@app.route("/pnjson/<nodeId>")
def pnjson(nodeId):
    pnjson = request_pg('sql/list_pnj_json.sql', (nodeId, nodeId))
    response = app.response_class(
        response=json.dumps(pnjson[0]['get_tree'][0]),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route("/pnjsonorg/<nodeId>")
def pnjsonorg(nodeId):
    pnjson = request_pg('sql/list_pnj_org_json.sql', (nodeId,))
    response = app.response_class(
        response=json.dumps(pnjson[0]['data']),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route("/pnjdetail/<pnjId>")
def pnjdetail( pnjId):
    pnj=request_pg('sql/detail_pnj.sql', (pnjId,))
    childrenList = request_pg('sql/list_childrens.sql', (pnjId,))
    relations= request_pg('sql/list_relations.sql', (pnjId, pnjId))
    return render_template('pnjdetail.html', pnjs=childrenList, relations= relations, pnj=pnj[0])


@app.route("/addrelation/<pnjId>", methods=['GET', 'POST'])
def addrelation( pnjId):
    if request.method=="GET":
        pnj=request_pg('sql/detail_pnj.sql', (pnjId,))
        parentsList = request_pg('sql/list_pnj.sql',(pnj[0]['nodeid'],))
        return render_template('addRelation.html', pnj=pnj[0], parentsList=parentsList)
    elif request.method=="POST":
        form =request.form # A form bound to the POST data
        if is_valid_relation(form):  # All validation rules pass
            # save in database
            write_pg('sql/add_relation.sql', (form["name"], pnjId, form.get("pnj2", type=int)))
            return json.dumps({'success':True, 'redirect':'/pnjdetail/'+pnjId}), 200, {'ContentType':'application/json'}
        else:
            form = request.form  # An unbound form
            return render_template('addPnj.html', form=form)


@app.route("/editPnj/<pnjId>", methods=['GET', 'POST'])
def editPnj( pnjId):
    if request.method=="GET":
        pnj=request_pg('sql/detail_pnj.sql', (pnjId,))
        parentsList = request_pg('sql/list_pnj.sql',(pnj[0]['nodeid'],))
        return render_template('editPnj.html', parentsList=parentsList, pnj=pnj[0])
    elif request.method=="POST":
        form =request.form # A form bound to the POST data
        if is_valid(form):  # All validation rules pass
            # save in database
            write_pg('sql/update_pnj.sql', (form["name"], form["photoUrl"], form["description"], form.get("parentID", type=int), pnjId))
            return json.dumps({'success':True, 'pnjid':pnjId, 'redirect':'/pnjdetail/'+pnjId}), 200, {'ContentType':'application/json'}
        else:
            form = request.form  # An unbound form
            return render_template('editPnj.html', pnj=form)
        
    
@app.route("/deletePnj/<pnjId>")
def deletePnj( pnjId):
    pnj=request_pg('sql/detail_pnj.sql', (pnjId,))[0]
    write_pg('sql/delete_pnj.sql', (pnjId,))
    return redirect('/pnjdetail/'+str(pnj['parentid']))

@app.route("/deleterelation/<pnjId>/<relationId>")
def deleterelation(pnjId, relationId):
    write_pg('sql/delete_relation.sql', (relationId,))
    return redirect('pnjdetail/'+pnjId)

@app.route("/org/<nodeId>")
def org(nodeId):
    pnj = request_pg('sql/detail_pnj.sql', (nodeId,))
    return render_template('organigramme.html', pnj=pnj[0])


@app.route("/addPnj/<parentId>", methods=['GET', 'POST'])
def addPnj(parentId):
    if request.method=="GET":
        parent= request_pg('sql/detail_pnj.sql', (parentId,))[0]
        parentsList = request_pg('sql/list_pnj.sql',(parent['nodeid'],))
        return render_template('addPnj.html', parentid=parent['id'], parentsList=parentsList)
    elif request.method=="POST":
        form =request.form # A form bound to the POST data
        if is_valid(form):  # All validation rules pass
            # save in database
            write_pg('sql/add_pnj.sql', (form["name"], form["photoUrl"], form["description"], form.get("parentID", type=int)))
            return json.dumps({'success':True, 'redirect':'/listPnj'}), 200, {'ContentType':'application/json'}
        else:
            form = request.form  # An unbound form
        return json.dumps({'success':False}), 400, {'ContentType':'application/json'}

if __name__ == "__main__":
    app.run(host="localhost", port="5333")
