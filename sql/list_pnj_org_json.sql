with recursive pnj_from_parents as
(
      select id, name, parentid, ARRAY[]::int[] as path, 0 as level
        from pnj
       where parentid is NULL and nodeid in (%s)

   union all

      select c.id, c.name, c.parentid, path || c.parentid, level
        from      pnj_from_parents p
             join pnj c
               on c.parentid = p.id
       where not c.id = any(path)
)
select array_to_json(array_agg(row_to_json(t))) as data
    from (
    select id, name, parentid, array_to_string(path,',','')
  from pnj_from_parents
) t
