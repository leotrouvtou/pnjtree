drop table if exists pnj cascade;
CREATE TABLE pnj
(
ID SERIAL PRIMARY KEY,
Name VARCHAR(100),
PhotoURL TEXT,
Description TEXT,
ParentID integer,
FOREIGN KEY (ParentID) REFERENCES pnj(id),
nodeid integer,
FOREIGN KEY (nodeid) REFERENCES pnj(id),
childrensID integer[]
);


CREATE OR REPLACE FUNCTION update_childrens()
 RETURNS trigger AS
$BODY$
BEGIN
    update pnj set childrensID = (select array_agg(id) from pnj where parentID = new.parentID) where id = new.parentId;
    update pnj set nodeid = (select nodeid from pnj where id = new.parentID) where id=new.id and new.parentID is not NULL;
    update pnj set nodeid = id where id=new.id and new.parentID is NULL;
    return new;
END;
$BODY$ LANGUAGE plpgsql;

create trigger update_childrens after INSERT
ON pnj
FOR each ROW
EXECUTE PROCEDURE update_childrens();

create trigger update_childrens after update
ON pnj
FOR each ROW
EXECUTE PROCEDURE update_childrens();

create trigger update_childrens after delete
ON pnj
FOR each ROW
EXECUTE PROCEDURE update_childrens();

drop table if exists relation;
CREATE TABLE relation
(
id SERIAL PRIMARY KEY,
name VARCHAR(100),
pnj1 integer,
pnj2 integer,
FOREIGN KEY (pnj1) REFERENCES pnj(id),
FOREIGN KEY (pnj2) REFERENCES pnj(id)
);

INSERT INTO pnj (parentid, name, description, id) VALUES (NULL, 'Cthulhu', 'Par-Delà les Montagnes Hallucinées',1);
INSERT INTO pnj (parentid, name, description, id) VALUES (1,'SS GABRIELLE','Notre Bateau',2);
INSERT INTO pnj (parentid, name, description, id) VALUES (2,'Officiers','Liste des officiers',3);
INSERT INTO pnj (parentid, name, description, id) VALUES (3,'Henry Vredenburgh','Capitaine',4);
INSERT INTO pnj (parentid, name, description, id) VALUES (3,'Paul Turlow','Capitaine en Second',5);
INSERT INTO pnj (parentid, name, description, id) VALUES (3,'Arthur Ballard','Officier en Second',6);
INSERT INTO pnj (parentid, name, description, id) VALUES (3,'Lamont Quigley','Troisieme Officier',7);
INSERT INTO pnj (parentid, name, description, id) VALUES (3,'John "Jack" Driscoll','Quatrieme Officier',8);
INSERT INTO pnj (parentid, name, description, id) VALUES (3,'Ray Lansing','Medecin de Bord',9);
INSERT INTO pnj (parentid, name, description, id) VALUES (3,'Charles Drummond','Chef Mecanicien',10);
INSERT INTO pnj (parentid, name, description, id) VALUES (3,'Bert Pacquard','Mecanicien en second',11);
INSERT INTO pnj (parentid, name, description, id) VALUES (2,'Ingenieurs Mecaniciens','Liste des Mecaniciens du  du SS Gabrielle',12);
INSERT INTO pnj (parentid, name, description, id) VALUES (12,'William Wheeler','',13);
INSERT INTO pnj (parentid, name, description, id) VALUES (12,'Mark Folsom','',14);
INSERT INTO pnj (parentid, name, description, id) VALUES (12,'Clyde Abernathy','',15);
INSERT INTO pnj (parentid, name, description, id) VALUES (2,'Ouvriers Machines','Liste des ouvriers marchines du ss gabrielle',16);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Tom Humphries','',17);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Bartholomew White','',18);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Sedney Beakins','',19);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Phillipe Brunel','',20);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Samuele Girolamo','',21);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Michael Fitzpatrick','',22);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Albert Webb','',23);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Richard Hartz','',24);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Carford Montaigne','',25);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Edgar Cawley','',26);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Sanley Rupert','',27);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Gregory Stanislaw','',28);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Lucius Morelli','',29);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Wylie Loden','',30);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Hugh O''Toole','',31);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Robert MacIlvaine','Operateur Radio',32);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Lysander Bertoli','Charpentier',33);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Roger Blunt','Maitre d''Equipage',34);
INSERT INTO pnj (parentid, name, description, id) VALUES (16,'Thomas Price','Magasinier',35);
INSERT INTO pnj (parentid, name, description, id) VALUES (2,'Quartiers-Maîtres','',36);
INSERT INTO pnj (parentid, name, description, id) VALUES (36,'Michael Oates','',37);
INSERT INTO pnj (parentid, name, description, id) VALUES (36,'Darren Horst','',38);
INSERT INTO pnj (parentid, name, description, id) VALUES (36,'Gregory Houlihan','',39);
INSERT INTO pnj (parentid, name, description, id) VALUES (2,'Matelots brevetés','',40);
INSERT INTO pnj (parentid, name, description, id) VALUES (40,'Peter Stokeley','',41);
INSERT INTO pnj (parentid, name, description, id) VALUES (40,'Abelard Almondale','',42);
INSERT INTO pnj (parentid, name, description, id) VALUES (40,'Truman Cotter','',43);
INSERT INTO pnj (parentid, name, description, id) VALUES (40,'Gordon Cooke','',44);
INSERT INTO pnj (parentid, name, description, id) VALUES (40,'Nicolas Pellerin (aka Thierry Gentil)','',45);
INSERT INTO pnj (parentid, name, description, id) VALUES (40,'Alexander Moseley','',46);
INSERT INTO pnj (parentid, name, description, id) VALUES (40,'David Waters','',47);
INSERT INTO pnj (parentid, name, description, id) VALUES (40,'Jude Pierce','',48);
INSERT INTO pnj (parentid, name, description, id) VALUES (40,'Chipper Green','',49);
INSERT INTO pnj (parentid, name, description, id) VALUES (40,'Judas Whitney','Intendant en Chef',50);
INSERT INTO pnj (parentid, name, description, id) VALUES (2,'Stewards','',51);
INSERT INTO pnj (parentid, name, description, id) VALUES (51,'Niles Abraham (cuisinier)','Cuisinier',52);
INSERT INTO pnj (parentid, name, description, id) VALUES (51,'Adam Henning','garcon de salle / cuisinier)',53);
INSERT INTO pnj (parentid, name, description, id) VALUES (51,'Philip Coates (garcon de salle / cuisinier)','garcon de salle / cuisinier)',54);
INSERT INTO pnj (parentid, name, description, id) VALUES (51,'David Lyle (blanchisseur)','Blanchisseur',55);
INSERT INTO pnj (parentid, name, description, id) VALUES (51,'Agust Wylie (blanchisseur).','Blanchisseur',56);
INSERT INTO pnj (parentid, name, description, id) VALUES (1,'ESM','EXPEDITION STARKWEATHER MOORE',57);
INSERT INTO pnj (parentid, name, description, id) VALUES (57,'ESM (Cadres)','',58);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'James Starkweather ','Conferencier / Guide / Explorateur – Dirigeant de l''ESM – (43 ans)',59);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'William Moore ','Geologue / Paleontologue – Dirigeant de l''ESM – (38 ans)',60);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Peter Sykes ','Guide Polaire – (34 ans)',61);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Gunnar Sorensen ','Guide polaire / Alpiniste – (36 ans)',62);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Nils Sorensen','Guide polaire / Alpiniste – (38 ans)',63);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Willard Griffith ','Geologue (Unviversité de Cornell) – (34 ans)',64);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Charlie Porter ','Assistant de W. Griffith – (40 ans)',65);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Morehouse Bryce ','Paleontologue (Université de Californie) – (29 ans)',66);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Timothy Cartier ','Assistant de M. Bryce – (29 ans)',67);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Charles Myers ','Archeologue (Université de Chicago) – (34 ans)',68);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Avery Giles ','Assistant de C. Myers - (21 ans)',69);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Pierce Albermarle ','Meteorologue (Université d''Oberlin) – (33 ans)',70);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Douglas Orgelfinger','Assitant de P. Albermarle –  (26 ans)',71);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Samuel Winslow ','Etudiant 3eme Cycle Glaciologie – (26 ans)',72);
INSERT INTO pnj (parentid, name, description, id) VALUES (58,'Docteur Richard Greene ','Medecin – (27 ans)',73);
INSERT INTO pnj (parentid, name, description, id) VALUES (57,'ESM (Equipe de Camp)','',74);
INSERT INTO pnj (parentid, name, description, id) VALUES (74,'Tomas Lopez ','Manoeuvre – (24 ans)',75);
INSERT INTO pnj (parentid, name, description, id) VALUES (74,'Hidalgo Cruz','Manoeuvre –  (38 ans)',76);
INSERT INTO pnj (parentid, name, description, id) VALUES (74,'Maurice Cole ','Manoeuvre – (19 ans)',77);
INSERT INTO pnj (parentid, name, description, id) VALUES (74,'David Packard ','Chef d''Equipe / Co Responsable de la sécurité – (28 ans)',78);
INSERT INTO pnj (parentid, name, description, id) VALUES (57,'ESM (Techniciens)','',79);
INSERT INTO pnj (parentid, name, description, id) VALUES (79,'Louis Laroche ','Operateur Radio / Technicien Electricien – (34 ans)',80);
INSERT INTO pnj (parentid, name, description, id) VALUES (79,'Albert Gilmore ','Technicien de Forage – (37 ans)',81);
INSERT INTO pnj (parentid, name, description, id) VALUES (79,'Michael O''Doul ','Technicien de Forage – (29 ans)',82);
INSERT INTO pnj (parentid, name, description, id) VALUES (79,'Gregor Pulaski ','Chef d''equipe de traineau – (35 ans)',83);
INSERT INTO pnj (parentid, name, description, id) VALUES (79,'Enke Fiskarson ','Maitre Chien – (24 ans)',84);
INSERT INTO pnj (parentid, name, description, id) VALUES (79,'Olav Snabjorn ','Maitre Chien – (36 ans)',85);
INSERT INTO pnj (parentid, name, description, id) VALUES (57,'ESM (Pilotes et Mecaniciens)','',86);
INSERT INTO pnj (parentid, name, description, id) VALUES (86,'Douglas Halperin ','Pilote – (30 ans)',87);
INSERT INTO pnj (parentid, name, description, id) VALUES (86,'Ralph Dewitt ','Pilote – (35 ans)',88);
INSERT INTO pnj (parentid, name, description, id) VALUES (86,'Laurence Longfellow ','Ingenieur / Mecanicien – (40 ans)',89);
INSERT INTO pnj (parentid, name, description, id) VALUES (86,'Alan "Colt" Hudson','Ingenieur / Mecanicien -- (31 ans)',90);
INSERT INTO pnj (parentid, name, description, id) VALUES (86,'Patrick Miles ','Technicien / Mecanicien Avion – (33 ans)',91);
INSERT INTO pnj (parentid, name, description, id) VALUES (NULL, 'Qin', '',92);
INSERT INTO pnj (parentid, name, description, id) VALUES (92, 'Nao', '',93);
INSERT INTO pnj (parentid, name, description, id) VALUES (
93,'Sun Wong Lo','Gouverneur de Nao (Zhao)',94);
INSERT INTO pnj (parentid, name, description, id) VALUES (93,'Ting Sen Liao',' Magistrat de Nao (Zhao)',95);
INSERT INTO pnj (parentid, name, description, id) VALUES (93,'Gao Dao "Epée de Bronze"','Militaire Officier vétéran du Zhao. Commande la garnison de soldats (principalement des cavaliers) basée à Nao et en charge dassurer la sécurité des routes commerciales de la région',96);
INSERT INTO pnj (parentid, name, description, id) VALUES (93,'Main Gauche','Shifu à Nao',97);
INSERT INTO pnj (parentid, name, description, id) VALUES (93,'Tong','Wu Xia Fils adoptif de Main Gauche',98);
INSERT INTO pnj (parentid, name, description, id) VALUES (93,'Yii','Fille adoptive de Main Gauche',99);
INSERT INTO pnj (parentid, name, description, id) VALUES (93,'Zu Zuo','Mercenaire Garde du corps de Lao Li',100);
INSERT INTO pnj (parentid, name, description, id) VALUES (93,'Bec Rouge','Militaire Jeune capitaine du Zhao. A la tête du détachement de soldat qui escorte Lao Li envisite dans la ville de Nao.',101);
INSERT INTO pnj (parentid, name, description, id) VALUES (93,'Lao Li','Erudit Dirige la bibliothèque historique du Roi du Zhao ',102);

ALTER SEQUENCE pnj_id_seq RESTART 103;
