  WITH RECURSIVE pnjs_materialized_path AS (
    SELECT id, name, description, ARRAY[]::INTEGER[] AS path
    FROM pnj WHERE parentid IS NULL

    UNION ALL

    SELECT pnj.id, pnj.name, pnj.description, pnjs_materialized_path.path || pnj.parentid
    FROM pnj, pnjs_materialized_path
    WHERE pnj.parentid = pnjs_materialized_path.id
  ) SELECT id, name, description, ARRAY[]::INTEGER[] AS children FROM pnjs_materialized_path WHERE $1 = pnjs_materialized_path.path[array_upper(pnjs_materialized_path.path,1)] -- some column polish for a cleaner JSON
