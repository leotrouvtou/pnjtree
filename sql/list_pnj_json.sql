CREATE OR REPLACE FUNCTION get_children(genre_id integer)
RETURNS json AS $$
DECLARE
result json;
BEGIN
SELECT array_to_json(array_agg(row_to_json(t))) INTO result -- inject output into result variable
FROM ( -- same CTE as above
  WITH RECURSIVE pnjs_materialized_path AS (
  select id, name, description, parentid, ARRAY[]::int[] as path, 0 as level
        from pnj
       where id = %s

   union all

      select c.id, c.name, c.description, c.parentid, path || c.parentid, level+1
        from      pnjs_materialized_path p
             join pnj c
               on c.parentid = p.id
       where not c.id = any(path)
  ) SELECT id, name, description, level, ARRAY[]::INTEGER[] AS children FROM pnjs_materialized_path WHERE $1 = pnjs_materialized_path.path[array_upper(pnjs_materialized_path.path,1)] order by level desc-- some column polish for a cleaner JSON
) t;
RETURN result;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION get_tree(data json) RETURNS json AS $$

var root = [];

for(var i in data) {
  build_tree(data[i]['id'], data[i]['name'], data[i]['description'],  data[i]['children']);
}

function build_tree(id, name, description, children) {
  var exists = getObject(root, id);
  if(exists) {
       exists['children'] = children;
  }
  else {
    root.push({'id': id, 'name': name, 'description': description, 'children': children});
  }
}


function getObject(theObject, id) {
    var result = null;
    var maxid = 0;

   for(obj in theObject){
        if (obj.id > maxid) maxid = obj.id;
    };
    if(theObject instanceof Array) {
    for(var i = 0; i < theObject.length; i++) {
        result = getObject(theObject[i], id);
        if (result) {
        break;
        }
    }
    }
    else
    {
    for(var prop in theObject) {
        if(prop == 'id') {
        if(theObject[prop] === id) {
            return theObject;
        }
        }
        if(theObject[prop] instanceof Object || theObject[prop] instanceof Array) {
        result = getObject(theObject[prop], id);
        if (result) {
            break;
        }
        }
    }
    }
    return result;
}

    return JSON.stringify(root);
$$ LANGUAGE plv8 IMMUTABLE STRICT;

WITH data AS(
select array_to_json(array_agg(row_to_json(t))) as data
    from (
     SELECT id, name, description, COALESCE(get_children(id), '[]') as children from pnj where nodeid= %s order by id asc
    ) t
) SELECT get_tree(data) from data;
